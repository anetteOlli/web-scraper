const puppeteer = require('puppeteer');
const fs = require('fs');

const LINK = 'https://no.wikipedia.org/wiki/JavaScript';
const FILENAME = 'jsonobject.json';

/**
 * Scrapes a given wikipedia page
 * @param {String} wikipediaPage url of the wikipedia page to be scraped
 * @returns {JSON content} json object containing title, ingress and links
 */
const scrapeWeb = async (wikipediaPage) => {
  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.goto(wikipediaPage, {
      waitUntil: 'networkidle2',
    });

    const content = await page.evaluate(() => {
      let dataLinks = [];
      const links = document.querySelectorAll('a');
      links.forEach((link) => {
        const href = link.getAttribute('href');
        if (href) {
          if (href.indexOf('http') != -1) {
            dataLinks.push(link.getAttribute('href'));
          }
        }
      });

      return {
        header: document.querySelector('h1.firstHeading').innerText,
        ingress: document.querySelector('p').innerText,
        links: dataLinks,
      };
    });
    await browser.close();
    return content;
  } catch (error) {
    throw error;
  }
};

/**
 * Saves a json object to a specified file.
 * @param {JSON object} jsonObject the object that wille be written to a file.
 * @param {String} filename outputfilename
 */
const saveJSONobjectToFile = async (jsonObject, filename) => {
  try {
    await fs.promises.writeFile(filename, JSON.stringify(jsonObject));
  } catch (error) {
    console.log(`Error occured while writing file: ${error.message}`);
  }
};

(async () => {
  try {
    const content = await scrapeWeb(LINK);
    await saveJSONobjectToFile(content, FILENAME);
  } catch (error) {
    console.log(error.message);
  }
})();
